package figury;

public interface UI {
	public void print(Object o);
	public void println(Object o);
	public Object read();
}
