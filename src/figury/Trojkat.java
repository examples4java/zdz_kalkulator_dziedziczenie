package figury;

import java.lang.reflect.Array;

public class Trojkat implements Figura {
	private double boki[],h;
	private int index;
	
	public Trojkat() {
		boki = new double[3];
		index = 0;
	}
	
	@Override
	public void dodajBoki(int boki) {
		// TODO Auto-generated method stub
	}

	@Override
	public Object zwrocBoki() {
		return this.boki;
	}

	@Override
	public double podajObwod() {
		// TODO Auto-generated method stub
		//double wynik=boki[0]+boki[1]+boki[2];
		
		return boki[0]+boki[1]+boki[2];
	}

	@Override
	public double podajPole() {
		// TODO Auto-generated method stub
		
		return 0;
	}

	@Override
	public void podajDlugoscBoku(int bok, double dlugosc) {
		// TODO Auto-generated method stub
		if (bok<3)
			boki[bok]=dlugosc;
	}

	@Override
	public void podajKolejnyBok(double dlugosc) {
		// TODO Auto-generated method stub
		podajDlugoscBoku(index++, dlugosc);
		//if (index<3)
		//	boki[index++]=dlugosc;
	}

	@Override
	public void podajBoki(double... boki) {
		// TODO Auto-generated method stub
		for (int i =0;i<Array.getLength(boki);i++) {
			podajKolejnyBok(boki[i]);
		}
	}
	
	public String toString() {
		String s="{";
		for (double d : boki) {
			s+=d + ", ";
		}
		return s.substring(0, s.length()-2)+"}";
	}

}
