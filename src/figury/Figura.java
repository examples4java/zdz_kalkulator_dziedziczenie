package figury;

/*
 * Interfejsy w j�zyku Java maj� za zadanie ujednolica� dzia�ania
 * w ramach r�nych klas co do ich funkcjonalno�ci.
 * Innymi s�owy w intefejsie zawiera si� szereg deklaracji 
 * metod (deklaracja => tylko wymienienieni nazwy, parametr�w i ewentualnie zwracanego typu)
 * 
 * Interfeksy WYMUSZ� na klasa, kt�re je implementuj� zawarcie wszystkich metod,
 * jakie deklaruj�.
 */
public interface Figura {
	public void dodajBoki(int boki);
	public Object zwrocBoki();
	public String toString();
	public double podajObwod(); //podaje wyliczenie obwodu figury
	public double podajPole(); //podaje wyliczenie pola figury
	public void podajDlugoscBoku(int bok, double dlugosc);
	public void podajKolejnyBok(double dlugosc);
	public void podajBoki(double... boki); //odpowiednik double boki[], ale umo�liwia podawanie warto�ci po przecinsku
	//przyk�ad prostok�tu: podajBoki(4,5)
	//w przypadku metody:
	//public void podajBoki(double boki[]);
	//przyk�ad prostok�tu: podajBoki({4,5})
}
