package figury;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Wielokat implements Figura {

	List<Double> boki;
	
	class Test {
		List<Double> boki;
	}
	
	public Wielokat() {
		boki = new ArrayList<>();
	}
	
	@Override
	public void dodajBoki(int boki) {
		// TODO Auto-generated method stub
	}

	@Override
	public Object zwrocBoki() {
		// TODO Auto-generated method stub
		String s=null;
		for (double d: boki) 
			s=d+ " ";
		return s;
	}
	
	public Object zwrocBok(int index) {
		return boki.get(index);
	}

	@Override
	public double podajObwod() {
		// TODO Auto-generated method stub
		double w=0;
		for (double d: boki)
			w+=d;
		return w;
	}

	@Override
	public double podajPole() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void podajDlugoscBoku(int bok, double dlugosc) {
		// TODO Auto-generated method stub
		podajKolejnyBok(dlugosc);
	}

	@Override
	public void podajKolejnyBok(double dlugosc) {
		// TODO Auto-generated method stub
		this.boki.add(dlugosc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void podajBoki(double... boki) {
		//double != Double
		this.boki.addAll(DoubleStream.of(boki).boxed().toList());
		
		//for(double d : boki) 
		//	this.boki.add(d);
	}
	
	
	
	public String toString() {
		String s="{";
		for (double d : boki)
			s+=d+", ";
		return s.substring(0,s.length()-2) + "}";
	}

}
